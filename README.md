![picture](https://i.imgur.com/ulU6Tmh.png)

Unofficial Twitch client, Open source and multi-platform for all platforms to use. Watch and be part of the action just like on the web version all wrapped up into a desktop application!

&nbsp;&nbsp;&nbsp;&nbsp;

![picture](https://i.imgur.com/R2YC5Lg.png)

![picture](https://i.imgur.com/ceforCm.png)

Even manage and run your streams conveniently from the Twitch application.

![picture](https://i.imgur.com/M0OVHOn.png)

&nbsp;&nbsp;&nbsp;&nbsp;

  You can install Twitch from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/twitch)

### Extensions / User Scripts

Load unpacked Chrome extensions or User Scripts from your application data directory.

Windows:

* %APPDATA%\Twitch\Extensions
* %APPDATA%\Twitch\Scripts

Linux

* $XDG_CONFIG_HOME/Twitch/Extensions or ~/.config/Twitch/Extensions
* $XDG_CONFIG_HOME/Twitch/Scripts or ~/.config/Twitch/Scripts

macOS

* ~/Library/Application/Twitch/Extensions
* ~/Library/Application/Twitch/Scripts

Place the unpacked extension folders in the Extensions directory and User Scripts (as JavaScript files) in the Scripts directory and they will be automatically loaded on startup.

### Download For All platforms (Linux, Mac OS and Windows)
  
  [Click to get the latest release](https://gitlab.com/twitch-application/application/-/releases)

### Author

* Corey Bruce

#### Contributors

* Brian Allred